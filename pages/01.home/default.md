---
title: Home
---

## It's as easy as 1-2-3 to start sharing your OER content!

1. Run the [setup wizard for the GitSync Plugin](../../admin/plugins/git-sync) and follow the provided instructions.
2. Copy the URL for your Git repository when viewing or editing a file in the 'pages' folder and configure the ['View/Edit Page in Git' Particle](../../admin/gantry/menu/) in the site menu.
3. If you would like, you can also [change the Creative Commons (CC) License](../../admin/gantry/configurations/default/layout) displayed on every page.

Since your content is now available within a Git repository (i.e. [GitHub](https://github.com/) or [GitLab](https://about.gitlab.com/)), collaborative authoring is also now possible, as shown below:

<iframe width="560" height="315" src="https://www.youtube.com/embed/5AG4DiVw9wM" frameborder="0" allowfullscreen></iframe>
